function isEven(num) {
    if (!num) {
        return true;
    }
    if (num === 1) {
        return false;
    }
    
    return isEven(num - 2);
}