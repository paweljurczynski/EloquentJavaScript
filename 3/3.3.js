// =============== COUNTING B ======================

function countBs(str) {
    var counter = 0;
    for (var i = 0; i < str.length; i++) {
        if (str[i] === 'B') {
            counter++;
        }
    }
    return counter;
};

function countBs(str) {
    return (str.match(/B/g) || []).length;
};

function countBs(str) {
    return str.split('B').length - 1;
};

function countBs(str) {
    var word = str.split('');
    var times = 0;

    word.map(function (letter) {
        if (letter === 'B') {
            times++;
        }
    });

    return times;
};

function countBs(str) {
    var word = str.split('');
    var times = 0;

    word.forEach(function (letter) {
        if (letter === 'B') {
            times++;
        }
    });

    return times;
};

function countBs(str) {
    var word = str.split('');
        
    var times = word.reduce(function (acc, current) {
        if(current === 'B'){
            acc++;
        }
        return acc;
    }, 0);

    return times;
};

function countBs(str) {
    var word = str.split('');
        
    var matched = word.filter(function (letter) {
        return letter === 'B';
    }, 0);

    return matched.length;
};

// =============== COUNTING CHAR ======================

function countChar(str, letter) {
    var counter = 0;
    for (var i = 0; i < str.length; i++) {
        if (str[i] === letter) {
            counter++;
        }
    }
    return counter;
};

function countChar(str, letter) {
    return str.split(letter).length - 1;
};

function countChar(str, letter) {
    return (str.match(new RegExp(letter, 'g')) || []).length;
};

// ==================== MIX =====================

function countBsOrOther(str, letter) {
    return str.split(letter || 'B').length - 1;
};

